#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
import os
from utils.bppReader import read_file, parameters_from_filename, select_files_instance, get_bpp_files
__author__ = "Nico_eisti"
__copyright__ = "Nico_eisti"
__license__ = "mit"

current_workdir = os.getcwd()

score_folder = current_workdir + "/benchmarks/score/"
bpp_folder = current_workdir + "/benchmarks/bin1data/"
score_file = score_folder + "score_by_instance.txt"

def test_read_file():
    assert read_file("/home/master/Documents/Metaheuristic_optim/binpacking/benchmarks/bin1data/N1C1W1_A.BPP") != []

def test_parameters_from_filename():
    assert parameters_from_filename("N1C2W1_A") == 2

def test_get_bpp_files():
    assert get_bpp_files(bpp_folder) != []

def test_select_files_instance():
    files_list = get_bpp_files(bpp_folder)
    assert select_files_instance(files_list, "A") != []