#!/usr/bin/env python
# -*- coding: utf-8 -*-

from argparse import ArgumentParser
import sys
import os


from binpacking import __version__
# from binpacking.bfd import ???
from utils.bppReader import _logger, read_file, parameters_from_filename, select_files_instance, get_bpp_files, get_score_dict
from binpacking.bin_packing_solver import bin_packing_solver

__author__ = "Nico_eisti"
__copyright__ = "Nico_eisti"
__license__ = "mit"


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list	
    """
    
    parser = ArgumentParser()
    args = parser.parse_args(args)
    _logger.info("Starting crazy calculations...")

    ###############  MAIN LOOP ################
    # Method to use
    method = "FFD" # FFD also available
    instance = "A"
    
    # Define the files
    current_workdir = os.getcwd()

    score_folder = current_workdir + "/benchmarks/score/"
    bpp_folder = current_workdir + "/benchmarks/bin1data/"
    score_file = score_folder + "score_by_instance.txt"
    files_list = get_bpp_files(bpp_folder)
    benchmark_files = select_files_instance(files_list, instance)

    nb_tests = len(benchmark_files)



    # Scoring
    score_dict = get_score_dict(score_file)

    tests_equal = 0
    tests_beaten = 0
    tests_lost = 0
    for filename in benchmark_files:
        # get parameters
        weight_interval, V_max = parameters_from_filename(filename)

        # get item weights
        item_weights = list(map(lambda x: int(x),read_file(bpp_folder + filename + ".BPP")))
        # get binpacking score
        lower_bound, upper_bound = weight_interval
        result, score = bin_packing_solver(item_weights, V_max, method, lower_bound=lower_bound, upper_bound=upper_bound)
        _logger.info("Vmax: " + str(V_max))
        _logger.info("Found: " + str(score))
        _logger.info("Benchmark: " + str(score))
        _logger.info(score_dict[filename])
        # get precision
        if score == score_dict[filename]:
            tests_equal += 1
        elif score < score_dict[filename]:
            tests_beaten += 1
        else:
            tests_lost += 1
    
    # Results on benchmarks
    _logger.info("List of files used for benchmark on instance " + instance)
    _logger.info(benchmark_files)
    _logger.info("Number of tests run: {:d}".format(nb_tests))
    _logger.info("Number of tests with same score: {:d}".format(tests_equal))
    _logger.info("Number of tests with better score: {:d}".format(tests_beaten))
    _logger.info("Number of tests with worse score: {:d}".format(tests_lost))
    _logger.info("Pourcentage of success {:.2f}%".format((tests_equal+tests_beaten)/nb_tests*100))
    _logger.info("Script ends here")


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
