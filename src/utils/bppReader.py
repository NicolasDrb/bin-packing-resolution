import logging
import io
import os

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
_logger = logging.getLogger(__name__)


def read_file(path):
    l = []
    with io.open(path, "r", newline=None) as fd:
        l = [line.replace("\n", "") for line in fd]
    return l


def get_score_dict(path):
    list_content = read_file(path)
    list_content = list(map(lambda x: x.replace("\n", ""), list_content))
    d = dict()
    i = 0
    while i < len(list_content) - 1:
        d[list_content[i]] = int(list_content[i+1])
        i += 2
    _logger.debug(d)
    return d


def parameters_from_filename(filename):
    """
        The instances are given names "NxCyWz_v.BPP" where
            x=1 (for n=50), x=2 (n=100), x=3 (n=200), x=4 (n=500)
            y=1 (for c=100), y=2 (c=120), y=3 (c=150)
            z=1 (for wj from [1,100]), z=2 ([20,100]), z=4 ([30,100])
            v=A..T for the 20 instances of each class 
x
        parameter	meaning
        n	number of items
        wj	weight (size) of item j (j=1,...,n)
        c	bin capacity
        m*	minimal number of bins

        Parameter setting
        parameter	values
        n	50, 100, 200, 500
        c	100, 120, 150
        wj	from [1,100], [20,100], [30,100] for j=1,...,n
    """
    volume_convert_to_value = [100,120,150]
    weight_convert_to_value = [(1,100),(20,100),(None, None),(30,100)]
    c = 0
    w = (None, None)
    # get the first part of the filename
    for i in range(len(filename) - 1):
        if filename[i] == "C":
            c = int(filename[i+1])
        if filename[i] == "W":
            w = int(filename[i+1])
    return weight_convert_to_value[w-1], volume_convert_to_value[c-1]


def get_bpp_files(path):
    files_list = os.listdir(path)
    _logger.debug(files_list)
    return files_list


def select_files_instance(files_list, instance_name):
    files_list = filter(lambda x: "_" + instance_name + ".BPP" in x, files_list)
    files_list = list(map(lambda x: x.split(".")[0], files_list))
    _logger.debug(files_list)
    return  files_list