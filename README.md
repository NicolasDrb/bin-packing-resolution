Binpacking
==========

L'objectif de ce projet est de pouvoir traiter les problèmes de bin packing 1D.

Installation
============

- Cloner le projet
- Aller à la racine du projet
- Entrer la commande suivante : `python setup.py install`
- Ensuite pour lancer le programme : `python src/main.py`


Description
===========

L'algorithme de résolution du bin packing se fait à l'aide de python (v 3.6).  
Nous utilisons les ressources suivantes pour notre phase de benchmarks:  
- [Data set 1 for BPP-1](https://www2.wiwi.uni-jena.de/Entscheidung/binpp/bin1dat.htm)

L'ensemble de l'implémentation de l'algorithme est disponible [ici](./src/binpacking/bin_packing_solver.py).  
Nous avons choisi d'utiliser l'instance A pour nos tests.  

Utiisations possibles
=====================

Par défaut, nous avons ajouté au projet tous les fichiers de benchmarks afin de pouvoir tester librement plusieurs instances.  
Vous pouvez choisir votre instance en allant dans le code du src/main.py en changeant "A" par "B" (ligne 32), par exemple 
(voir le [site](https://www2.wiwi.uni-jena.de/Entscheidung/binpp/bin1dat.htm)).

Par ailleurs, il est possible de changer l'algorithme utilisé pour la résolution.  
Nous avons implémenté deux algorithmes : BFD, FFD.  
Il suffit de changer "BFD" par "FFD" ligne 31 dans le main.py.





